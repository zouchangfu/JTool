//给文章加背景格子
function backgroundStyle() {
    var article = document.getElementsByTagName('article')[0]
    if (article) {
        article.style.cssText = ` background: -webkit-linear-gradient(top, transparent 19px, #ececec 20px), -webkit-linear-gradient(left, transparent 19px, #ececec 20px);
background-size: 20px 20px;`
    }
}

var colorText = ''


// 给 > 加绿边样式
function blockquote() {
    var blockquotes = document.getElementsByTagName('blockquote')
    if (blockquotes) {
        for (var i = 0; i < blockquotes.length; i++) {
            blockquote = blockquotes[i]
            blockquote.style.cssText = `padding: 10px 10px 10px 1rem; font-size: 0.9em; margin: 1em 0px; color: rgb(0, 0, 0); border-left: 5px solid ` + colorText + `; background: rgb(239, 235, 233);`
        }

    }
}



//给h2加样式
function h2() {

    var h2s = document.getElementsByTagName('h2')
    if (!h2s) {
        return
    }
    for (var i = 0; i < h2s.length; i++) {
        h2 = h2s[i]
            // h2.style.cssText = `padding-bottom: .3em;font-size: 1.75em;line-height: 1.225;border-bottom: 2px solid #10c921;`
        var text = h2.innerText
        h2.innerHTML = `<span style='font-size: inherit; line-height: inherit; margin: 0px; display: inline-block; font-weight: normal; background: ` + colorText + `; color: rgb(255, 255, 255); padding: 3px 10px 1px; border-top-right-radius: 3px; border-top-left-radius: 3px; margin-right: 3px;'>` + text + `</span>`
        h2.style.cssText = `color: inherit; line-height: inherit; padding: 0px; margin: 1.6em 0px; font-weight: bold; border-bottom: 2px solid ` + colorText + `; font-size: 1.3em;`
    }
}


//隐藏侧边栏
function sidebar() {
    var sidebar = document.getElementsByClassName('sidebar')[0]
    if (!sidebar) {
        return
    }
    sidebar.style.cssText = 'display:none;'

    var suspended_panel = document.getElementsByClassName('article-suspended-panel')[0]
    suspended_panel.style.cssText = 'display:none;'

}

//扩大文章宽度
function main_area() {

    var main_area1 = document.getElementsByClassName('main-area')[0]

    if (!main_area1) {
        return
    }
    main_area1.style.cssText = 'width:950px !important;'
}




function changeStyle() {
    backgroundStyle()
        // blockquote()
        // h2()

    chrome.storage.sync.get('color', function(data) {
        colorText = data.color
        blockquote();
        h2();
    })





    var h2Styles = document.querySelectorAll("code");

    if (h2Styles) {
        for (var i = 0; i < h2Styles.length; i++) {
            item = h2Styles[i]
                // console.info('style:', item)
            item.style.cssText = `color: #476582 ;`
        }
    }



    var codeStyles = document.querySelectorAll("pre")
    if (codeStyles) {
        for (var i = 0; i < codeStyles.length; i++) {
            item = codeStyles[i]
            item = item.querySelector('code')
            if (item) {
                item.style.cssText = `background: #282c34 !important;color: white !important;border-radius: 10px !important;`
            }

        }
    }


    var keywords = document.getElementsByClassName("hljs-keyword")
    if (keywords) {
        for (var i = 0; i < keywords.length; i++) {
            item = keywords[i]
            item.style.cssText = `color: #66d9ef;`
        }
    }

    //hljs-name
    var names = document.getElementsByClassName("hljs-name")
    if (names) {
        for (var i = 0; i < names.length; i++) {
            item = names[i]
            item.style.cssText = `color: #f92672;`
        }
    }

    //hljs-attr
    var attrs = document.getElementsByClassName("hljs-attr")
    if (attrs) {
        for (var i = 0; i < attrs.length; i++) {
            item = attrs[i]
            item.style.cssText = `color: #a6e22e;`
        }
    }

    //hljs-tag
    var tags = document.getElementsByClassName("hljs-tag")
    if (tags) {
        for (var i = 0; i < tags.length; i++) {
            item = tags[i]
            item.style.cssText = `color: white;`
        }
    }




    var titles = document.getElementsByClassName("hljs-title")
    if (titles) {
        for (var i = 0; i < titles.length; i++) {
            item = titles[i]
            item.style.cssText = `color: #e6db74;`
        }
    }


    var comments = document.getElementsByClassName("hljs-comment")
    if (comments) {

        for (var i = 0; i < comments.length; i++) {
            item = comments[i]
            item.style.cssText = ` color: #708090;`
        }
    }


    var strings = document.getElementsByClassName("hljs-string")
    if (strings) {
        for (var i = 0; i < strings.length; i++) {
            item = strings[i]
            item.style.cssText = `color: #a6e22e;`
        }
    }


}


setTimeout(function() { changeStyle() }, 1500);