$(function() {
    chrome.storage.sync.get('isOpen', function(data) {
        if (data.isOpen) {
            $('#open').html('点击关闭沉浸式阅读模式')
        } else {
            $('#open').html('点击开启沉浸式阅读模式')
        }
    })


    $('#open').click(function() {
        chrome.storage.sync.get('isOpen', function(data) {
            if (data.isOpen) {
                $('#open').html('点击开启沉浸式阅读模式')
                chrome.storage.sync.set({ 'isOpen': false })
                chrome.tabs.executeScript(null, { file: 'js/open.js' });
            } else {
                chrome.storage.sync.set({ 'isOpen': true })
                $('#open').html('点击关闭沉浸式阅读模式')
                chrome.tabs.executeScript(null, { file: 'js/open.js' });
            }
        })
    })

})