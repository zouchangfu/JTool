//隐藏侧边栏
function sidebar() {
    var sidebar = document.getElementsByClassName('sidebar')[0]
    if (!sidebar) {
        return
    }
    sidebar.style.cssText = 'display:none;'

    var suspended_panel = document.getElementsByClassName('article-suspended-panel')[0]
    suspended_panel.style.cssText = 'display:none;'

}

//扩大文章宽度
function main_area() {

    var main_area1 = document.getElementsByClassName('main-area')[0]

    if (!main_area1) {
        return
    }
    main_area1.style.cssText = 'width:950px !important;'
}

//显示侧边栏
function sidebar2() {
    var sidebar = document.getElementsByClassName('sidebar')[0]
    if (!sidebar) {
        return
    }
    sidebar.style.cssText = 'display:block;'

    var suspended_panel = document.getElementsByClassName('article-suspended-panel')[0]
    suspended_panel.style.cssText = 'display:block;'

}

//恢复原来文章宽度
function main_area2() {

    var main_area1 = document.getElementsByClassName('main-area')[0]

    if (!main_area1) {
        return
    }
    main_area1.style.cssText = 'width:700px !important;'
}


chrome.storage.sync.get('isOpen', function(data) {
    if (data.isOpen) {
        sidebar();
        main_area()
    } else {
        sidebar2();
        main_area2()
    }
})


var colorText = ''

chrome.storage.sync.get('color', function(data) {
    colorText = data.color
    blockquote();
    h2();
})




// 给 > 加绿边样式
function blockquote() {
    var blockquotes = document.getElementsByTagName('blockquote')
    if (blockquotes) {
        for (var i = 0; i < blockquotes.length; i++) {
            blockquote = blockquotes[i]
            blockquote.style.cssText = `padding: 10px 10px 10px 1rem; font-size: 0.9em; margin: 1em 0px; color: rgb(0, 0, 0); border-left: 5px solid ` + colorText + `; background: rgb(239, 235, 233);`
        }

    }
}



//给h2加样式
function h2() {

    var h2s = document.getElementsByTagName('h2')
    if (!h2s) {
        return
    }
    for (var i = 0; i < h2s.length; i++) {
        h2 = h2s[i]
            // h2.style.cssText = `padding-bottom: .3em;font-size: 1.75em;line-height: 1.225;border-bottom: 2px solid #10c921;`
        var text = h2.innerText
        h2.innerHTML = `<span style='font-size: inherit; line-height: inherit; margin: 0px; display: inline-block; font-weight: normal; background: ` + colorText + `; color: rgb(255, 255, 255); padding: 3px 10px 1px; border-top-right-radius: 3px; border-top-left-radius: 3px; margin-right: 3px;'>` + text + `</span>`
        h2.style.cssText = `color: inherit; line-height: inherit; padding: 0px; margin: 1.6em 0px; font-weight: bold; border-bottom: 2px solid ` + colorText + `; font-size: 1.3em;`
    }
}