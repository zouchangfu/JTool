 chrome.contextMenus.create({
     id: '1',
     title: '开启沉浸模式',
     documentUrlPatterns: ['https://*.juejin.cn/*'],
     onclick: function() {
         chrome.storage.sync.get('isOpen', function(data) {
             if (data.isOpen) {
                 chrome.storage.sync.set({ 'isOpen': false })
                 execute();
                 chrome.contextMenus.update('1', { title: '开启沉浸模式' })

             } else {
                 chrome.storage.sync.set({ 'isOpen': true })
                 execute();
                 chrome.contextMenus.update('1', { title: '关闭沉浸模式' })

             }
         })

     }
 });

 function execute() {
     chrome.tabs.executeScript(null, { file: 'js/open.js' });
 }


 chrome.contextMenus.create({
     type: 'normal', // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
     title: '默认主题', // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
     onclick: function() {
         chrome.storage.sync.set({ 'color': 'rgb(127, 127, 127)' })
         execute();
     }, // 单击时触发的方法
     documentUrlPatterns: ['https://*.juejin.cn/*'] // 只在某些页面显示此右键菜单
 });


 chrome.contextMenus.create({
     type: 'normal', // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
     title: '主题1', // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
     onclick: function() {
         chrome.storage.sync.set({ 'color': '#FFC0CB' })
         execute();
     }, // 单击时触发的方法
     documentUrlPatterns: ['https://*.juejin.cn/*'] // 只在某些页面显示此右键菜单
 });


 chrome.contextMenus.create({
     type: 'normal', // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
     title: '主题2', // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
     onclick: function() {
         chrome.storage.sync.set({ 'color': '#DB7093' })
         execute();
     }, // 单击时触发的方法
     documentUrlPatterns: ['https://*.juejin.cn/*'] // 只在某些页面显示此右键菜单
 });

 chrome.contextMenus.create({
     type: 'normal', // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
     title: '主题3', // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
     onclick: function() {
         chrome.storage.sync.set({ 'color': '#DA70D6' })
         execute();
     }, // 单击时触发的方法
     documentUrlPatterns: ['https://*.juejin.cn/*'] // 只在某些页面显示此右键菜单
 });

 chrome.contextMenus.create({
     type: 'normal', // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
     title: '主题4', // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
     onclick: function() {
         chrome.storage.sync.set({ 'color': '#9370DB' })
         execute();

     }, // 单击时触发的方法
     documentUrlPatterns: ['https://*.juejin.cn/*'] // 只在某些页面显示此右键菜单
 });

 chrome.contextMenus.create({
     type: 'normal', // 类型，可选：["normal", "checkbox", "radio", "separator"]，默认 normal
     title: '主题5', // 显示的文字，除非为“separator”类型否则此参数必需，如果类型为“selection”，可以使用%s显示选定的文本
     onclick: function() {
         chrome.storage.sync.set({ 'color': '#1E90FF' })
         execute();
     }, // 单击时触发的方法
     documentUrlPatterns: ['https://*.juejin.cn/*'] // 只在某些页面显示此右键菜单
 });